<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Multiple File Upload</title>
	<link rel="stylesheet" href="bootstrap.min.css">
	<style>
	.container {
		margin-top: 20px;
		background-color: bisque;
	}
	.success_msg {
		color: navy;
		background-color: #a6e1ec;
		font-weight: bold;
		margin-left: 10px;
	}
	input[type="file"] {
		float: left;
		padding: 2px;
	}
	input[type="submit"] {
		margin-bottom: 10px;
		float: left;
	}
	</style>
</head>
<body>

	<div class="container">
		<form action="process.php" method="POST" enctype="multipart/form-data">
			<?php 
				if (isset($_GET['success_msg'])) {
					echo "<span class='success_msg'>File Uploaded Successfully</span>";
				}
				else {
					echo "File Not Uploaded";
				}
	 		?>
			<input type="file" name="fileUpload[]" multiple="multiple">
			<input type="submit" value="Upload" name="file_upload">
		</form>
	</div>
	<div class="list">
		<table class="table">
			<tr>
				<th>Serial No</th>
				<th>File Name</th>
				<th>Action</th>
			</tr>
			<?php 

				$directory = opendir("uploads");
				if ($directory) {
					$i = 0;
					$j = $i-1;
					while(($fileList = readdir($directory)) !== false) {
						if ($fileList != '.' && $fileList != '..') {
							echo "<tr>";

							echo "<td>$j</td>";
							echo "<td><a href=\"uploads/$fileList\">$fileList</a></td>";
							echo "<td><a href='' onClick='delete()'>Delete</a></td>";
							echo "</tr>";
						}
						$i++;
						$j++;
					}
					closedir($directory);
				}

 			?>
		</table>
	</div>

</body>
</html>