<style>
#board {
float: left;
    width: 480px;
    height: 480px;
    border: 10px solid #888;
}
.square {
    float: left;
    width: 60px;
    height: 60px;
    color: #888;
}
.black {
    background-color: black;
}
.white {
    background-color: white;
}
</style>

<div id="board">

<?php

    for ($y=8;$y>0;$y--) {

        for ($x=0;$x<8;$x++) {
            echo (($x+$y)%2==0) ? "<div class=\"square white\">"."</div>" : "<div class=\"square black\">"."</div>";
        }
    }
?>
</div>